#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

#define SIZE 1024

void generate_matrix(double **matrix, int seed);
void print_matrix(double **matrix);
void save_matrix(double **matrix, const char *filename);
void gauss_inverse(double **matrix, double **inverse);

int main(int argc, char *argv[]) {
    double **matrix = (double **)malloc(SIZE * sizeof(double *));
    double **inverse = (double **)malloc(SIZE * sizeof(double *));
    for (int i = 0; i < SIZE; i++) {
        matrix[i] = (double *)malloc(SIZE * sizeof(double));
        inverse[i] = (double *)malloc(SIZE * sizeof(double));
    }

    double start_time, end_time;

    int seed = 42;
    generate_matrix(matrix, seed);

    start_time = omp_get_wtime();
    gauss_inverse(matrix, inverse);
    end_time = omp_get_wtime();

    printf("Time taken: %f seconds\n", end_time - start_time);

    char filename[100];
    sprintf(filename, "inverse_matrix_%d.txt", omp_get_max_threads());
    save_matrix(inverse, filename);

    for (int i = 0; i < SIZE; i++) {
        free(matrix[i]);
        free(inverse[i]);
    }
    free(matrix);
    free(inverse);

    return 0;
}

void generate_matrix(double **matrix, int seed) {
    srand(seed);
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            matrix[i][j] = (double)rand() / (RAND_MAX + 1.0);
            if (i == j) {
                matrix[i][j] = fabs(matrix[i][j]) + SIZE;
            }
        }
    }
}

void print_matrix(double **matrix) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            printf("%8.2f ", matrix[i][j]);
        }
        printf("\n");
    }
}

void save_matrix(double **matrix, const char *filename) {
    FILE *file = fopen(filename, "w");
    if (file == NULL) {
        perror("Unable to open file for writing");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            fprintf(file, "%lf ", matrix[i][j]);
        }
        fprintf(file, "\n");
    }
    fclose(file);
}

void gauss_inverse(double **matrix, double **inverse) {
    #pragma omp parallel for
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            inverse[i][j] = (i == j) ? 1.0 : 0.0;
        }
    }

    // Прямий хід методу Гаусса
    for (int k = 0; k < SIZE; k++) {
        double pivot = matrix[k][k];
        #pragma omp parallel for
        for (int j = 0; j < SIZE; j++) {
            matrix[k][j] /= pivot;
            inverse[k][j] /= pivot;
        }
        #pragma omp parallel for
        for (int i = 0; i < SIZE; i++) {
            if (i != k) {
                double factor = matrix[i][k];
                for (int j = 0; j < SIZE; j++) {
                    matrix[i][j] -= factor * matrix[k][j];
                    inverse[i][j] -= factor * inverse[k][j];
                }
            }
        }
    }
}
