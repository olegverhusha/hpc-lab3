#!/bin/bash
#PBS -N gauss_inverse
#PBS -l nodes=1:ppn=8
#PBS -l walltime=01:00:00
#PBS -j oe
#PBS -o output.log

cd $PBS_O_WORKDIR

gcc -std=c99 -fopenmp -o gauss_inverse gauss_inverse.c

times=()

for threads in {1..8}
do
    export OMP_NUM_THREADS=$threads
    echo "Running with $threads threads"
    start_time=$(date +%s.%N)
    ./gauss_inverse
    end_time=$(date +%s.%N)
    elapsed=$(echo "$end_time - $start_time" | bc)
    times+=($elapsed)
    echo "Time with $threads threads: $elapsed seconds"
done

echo "Threads,Time(s)" > results.out
for i in {0..7}
do
    echo "$(($i+1)),${times[$i]}" >> results.out
done
